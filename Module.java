class Module{

private String code;
private String name;
private int credits;
private ObjectArrayList lecturers;
private Staff coordinator;
private ObjectArrayList enrolledStudents;

public Module(String code, String name, int credits){
    this.code = code;
    this.name = name;
    this.credits = credits;
    lecturers = new ObjectArrayList();
    coordinator = null;
    enrolledStudents=new ObjectArrayList();
} 

/**
* Returns the code of the module
*/
public String getCode(){
     return code;
}

/**
* Returns the name of the module
*/
public String getName(){
     return name;
}

/**
* Returns the number of credits the module *represents
*/
public int getCredits(){
     return credits;
}

/**
* Returns the coordinator of the module
*/
public Staff getCoordinator(){
     return coordinator;
}

/**
* Adds a lecturer to the list of module lecturers
* @param lecturer - the lecturer to be aded
*/
public void addLecturer(Staff lecturer){
    if(!lecturers.contains(lecturer)){
         lecturers.add(lecturer);
         lecturer.addModule(this);
    }
}

/**
* Removes a lecturer from the list of module 
* lecturers
* @param lecturer The lecturer to be removed
* @return boolean denoting if the removal was
* successful
*/
public boolean removeLecturer(Staff lecturer){
     return lecturers.remove(lecturer);
}

/**
* Sets a staff member as module coordinator
* @param newCoordinator - the new coordinator
*/
public void setCoordinator(Staff newCoordinator){
     if(coordinator!=null){
     coordinator.removeModuleCoordinated(this);
      }
     coordinator = newCoordinator;
     coordinator.addModuleCoordinated(this);
}

/**
* Enrolls a student on the module and checks
* if the student is already enrolled and if the 120
* credits for the student are exceeded
*@param student - The student to be enrolled
*@return boolean denoting if the enrollment was
* successful
*/
public boolean enrollStudent(Student student){
     if((!enrolledStudents.contains(student))&&(student.getCredits() + credits<120)){
      enrolledStudents.add(student);
      student.addModule(this);
      return true;
     }
     else{
     return false;
     }
}
}

/**
* Removes a student from the module
*@return a boolean denoting the success of the
* removal
*/
public boolean removeStudent(Student student) {
     enrolledStudents.remove(student);
     return student.removeModule;
}

