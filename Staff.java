public class Staff {
  
    private String forename;
    private String surname;
    private int universityIdNumber;
    private ObjectArrayList modulesCoordinated, modulesLectured;
   
    public Staff (String forename, String surname, int universityIdNumber) {
    j    this.forename = forename;
        this.surname = surname;
        this.universityIdNumber = universityIdNumber;
   
        modulesCoordinated = new ObjectArrayList();
        modulesLectured = new ObjectArrayList();
   }
   
    public String getForename() {
        return Forename;
    } 
    
    String getSurname() {
        return Surname;
    }
    
    int getUniversityIdNumber() {
        return UniversityIdNumber;
    }  
    
    void addModulesCoordinated(Module m) {
        modulesCoordinated.add(m);
    }
    
    void addModulesLectured(Module m) {
        modulesLectured.add(m);
    }
    
    boolean removeModulesCoordinated(Module m) {
        return modulesCoordinated.remove(m);
    }
    
    boolean removeModulesLectured(Module m) {
        return modulesLectured.remove(m);
    }

   int NumberOfModulesLectured() {
       return modulesLectured;
   }   
}
