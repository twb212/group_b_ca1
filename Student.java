//Defines a stuent object

class Student{
	private String forename, surname, address;
	private int ID;
	ObjectArrayList modules; 

	/**
	 * Class constructor, creates a Student object according to parameters, with empty ObjectArrayList of modules
	 *
	 * @param forename String containing Student's forename
	 * @param surname String containing Student's surname
	 * @param ID int containing Student's ID number
	 * @param address String containing the Sudent's address
	 */
	public Student(String forename, String surname, int ID, String address){
		this.forename = forename;
		this.surname = surname;
		this.ID = ID;
		this.address = address;
		modules = new ObjectArrayList();
	}
	/** 
	 * Add Module m to ObjectArrayList modules if student's credits do not exceed 120
	 * returns true if successful
	 *
	 * @param m Module to be added to ObjectArrayList modules
	 * @return boolean indicating whether m has been added successfully 
	*/
	public boolean addModule(Module m){
		//Appends Module m to modules if number of credits does not exceed 120
		//Returns true if successful, false otherwise
		if((numberOfCredits() + m.getCredits())> 120)
			return false;
		modules.add(m);
		return true;
	}
	/**
	 * Remove Module m from ObjectArrayList, returns true if successful
 	 *
	 * @param m Module to be removed from ObjectArrayList modules
	 * @return boolean indicating whether m has been removed successfully
	*/
	public boolean removeModule(Module m){
		//Removes Module m from modules
		//Returns true if successful, false otherwise
		return modules.remove(m);
	}
	/**
	 * Calculates and returns the total number of credits that the Modules in modules are worth
	 *
	 * @return int containing the total number of credits that the Modules in module are worth
	*/
	public int numberOfCredits(){
		int numberOfCredits;
		for(int i = 0; i < modules.size(); i++){
			numberOfCredits += modules.get(i).getCredits();
		}
		return numberOfCredits;
	}
	/**
	 * Overrides equals method of Object class, returns true if each attribute of s is equal to each attribute of this
 	 *
	 * @param s Student to compare to this
	 * @return boolean indicating whether s and this are equal
	*/
	public boolean equals(Student s){
		//Overrides equals method
	}
	/**
	 * Access method for forename
  	 *
	 * @return String forename of this Student
	*/
	public String getForename(){
		return forename;
	}
	/**
	 * Mutator method for forename
	 *
	 * @param forename String containing student's forename 
	*/
	public void setForename(String forename){
		this.forename = forename;
	}
	/**
	 * Access method for surname
  	 *
	 * @return String surname of this Student
	*/
	public String getSurname(){
		return surname;
	}
	/**
	 * Mutator method for surname
	 *
	 * @param surname String containing student's surname
	*/
	public void setSurname(String surname){
		this.surname = surname;
	}
	/**
	 * Access method for ID
  	 *
	 * @return int ID of this Student
	*/
	public int getID(){
		return ID;
	}
	/**
	 * Mutator method for ID
	 *
	 * @param ID int containing student's ID 
	*/
	public void setID(int ID){
		this.ID = ID;
	}
	/**
	 * Access method for address
  	 *
	 * @return String address of this Student
	*/
	public String getAddress(){
		return address;
	}
	/**
	 * Mutator method for address
	 *
	 * @param address String containing student's address 
	*/
	public void setAddress(String address){
		this.address = address;
	}
}

