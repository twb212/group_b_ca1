
/**
 * ObjectArrayList -- class maintains a list of objects which may be appended
 * to, queried, and removed from.
 * <p>
 * Current implementation is rather inefficient in resizing -- can you think of 
 * how it might be improved?
 * 
 * @author Jonathan Fieldsend 
 * @version 1.0
 */
public class ObjectArrayList
{
    // Array list contents are stored in an array
    private Object[] list;
    
    /**
     * Class constructor makes an empty ObjectArrayList
     */
    public ObjectArrayList() {
        // default constructor initialises the array with zero elements
        this.list  = new Object[0];
    }
    
    /**
     * Class constructor copies (clones) an ObjectArrayList argument to set 
     * state of this ObjectArrayList
     * 
     * @param arrayList ObjectArrayList to set initial state
     */
    public ObjectArrayList(ObjectArrayList arrayList) {
        // copy constructor -- takes another instance of the same type, and 
        // copys across the array contents (values) of the argument to 
        // initialise this ObjectArrayList
        this.list  = arrayList.list.clone();
    }
    
    /**
     * Add Object argument to list
     * 
     * @param o Object to add to this list
     */
    public void add(Object o) {
        /* as I'm about to extend the list by one, I make a new array, which
         * is one element longer than the current array, and copy each element
         * of the current array across
         */
        Object[] temp = new Object[this.list.length+1];
        for (int i=0; i<this.list.length; i++) {
            temp[i] = this.list[i];
        }
        // add the new element to the end of the new array
        temp[this.list.length] = o;
        // set the value of the list attribute to refer to the new array, 
        // thus replacing the old array
        this.list = temp;
    }
    
    /**
     * Remove Object argument to list, returns true if Object has been removed,
     * returns false if Object argument was not in list 
     * 
     * @param o Object to remove from this list
     * @return  boolean denoting if the removed
     */
    public boolean remove(Object o) {
        // have an index to track where the Object argument lies in the 
        // current list
        int index = -1;
        for (int i=0; i<this.list.length; i++) {
            if (this.list[i].equals(o)){
                // if we have found the object, then record its location
                // and exit from the loop
                index = i;
                break;
            }
        }
        // now we want to remove the element from a particular location, so 
        // will use the overloaded remove method to do this 
        return this.remove(index);
    }
    
    /**
     * Remove Object from the given index in the list, returns true if Object has 
     * been removed, returns false if the index is out of the legal range
     * 
     * @param index index of Object to remove from list
     * @return  boolean denoting if removed
     */
    public boolean remove(int index) {
        // if the index is invalid (outside the legal range of the array)
        // then we cannot remove that element, so return false
        if (index<0 || index > this.list.length-1)
            return false;
        // if the index is valid, then first create a new array which is one 
        // element shorter than the current list, which will contain the list 
        // when the element has been removed
        Object[] temp = new Object[this.list.length-1];
        // copy accross all elements up to the element to be removed into the
        // new list
        for (int i=0; i<index; i++) 
            temp[i] = this.list[i];
        // copy across all element after the element to be removed into the new 
        // list
        for (int i=index+1; i<this.list.length; i++)
            temp[i-1] = this.list[i];
        // the new list now contains references to all the elements that were 
        // in the old list, except for the one to be removed, so now set the 
        // value of the list attribute to refer to this new list, and return 
        // true to indicate a sucessful removal 
        this.list=temp;
        return true;
    }
    
    /**
     * Replace Object at the given index in the list, with the Object argument.
     * Returns true if sucessifully replaced, returns false if the index is out 
     * of the legal range
     * 
     * @param o Object to put in list
     * @param index index of Object to replace in list
     * @return  boolean denoting if removed
     */
    public boolean replace(Object o, int index) {
        // first check index is legal, if not, return false
        if (index<0 || index>this.list.length-1)
            return false;
        // otherwise replace object stored at that index with the Object argument
        this.list[index] = o;
        return true;
    }
    
    /**
     * Check if Object is contained in list. Returns true if present in list,
     * otherwse returns false
     * 
     * @param o Object check in list
     * @return  boolean denoting if present in list
     */
    public boolean contains(Object o) {
        // iterate through the list and check if any element matches the argument
        for (Object member : this.list)
            if (member.equals(o))
                return true;
        // if I have reached this point, then no element matched the argument,
        // so I return false
        return false;
    }
    
    /**
     * Get list contents at index. Will return null if the index is out of the 
     * valid range of the list
     * 
     * @param index index of Object to get
     * @return  Object at index
     */
    public Object get(int index) {
        // check index is in legal range, if not, return null
        if (index<0 || index>this.list.length-1)
            return null;
        // otherwise return the object at that index in the array
        return this.list[index];
    }
    
    /**
     * Get the total number of elements of the list.
     * 
     * @return  Number of list elements
     */
    public int size() {
        // the number of elements in the list is simply the number of elements
        // of the array;
        return this.list.length;
    }
}
